﻿using UnityEngine;

namespace ScriptableObjects.Classes.Prefabs
{
    [CreateAssetMenu(fileName = "GameData", menuName = "GameData")]
    public class GameData : ScriptableObject
    {
        [field: SerializeField] public float DelayBetweenTaps { get; private set; }
        [field: SerializeField] public float TouchDistanceToHoldObject { get; private set; }
    }
}