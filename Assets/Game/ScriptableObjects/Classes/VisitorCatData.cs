﻿using UnityEngine;
using static Common.Enums;

namespace Game.ScriptableObjects.Classes
{
	[CreateAssetMenu(fileName = "VisitorCatData", menuName = "Cats/VisitorCatData", order = 0)]
	public class VisitorCatData : ScriptableObject
	{
		[SerializeField] private ResourceType _resourceType;
		[SerializeField] private float _addedValue = 10f;


		public ResourceType ResourceType => _resourceType;

		public float AddedValue => _addedValue;
	}
}