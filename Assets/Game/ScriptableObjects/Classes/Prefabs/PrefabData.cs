﻿using System;
using System.Collections.Generic;
using Common.ObjectPool;
using Game.Scripts.Commons.ObjectPool;
using Game.Scripts.Gameplay.Сats.MainCat;
using Game.Scripts.Gameplay.Сats.MainCat.Helper;
using Game.Scripts.Gameplay.Сats.Queue.VisitorCats;
using Game.Scripts.UI.Money;
using UnityEngine;
using static Common.Enums;

namespace ScriptableObjects.Classes.Prefabs
{
	[CreateAssetMenu(fileName = "PrefabData", menuName = "Prefabs/PrefabData", order = 0)]
	public class PrefabData : ScriptableObject
	{
		
		[SerializeField] private MainCat _mainCatPrefab;
		[SerializeField] private CatHelper _catHelper;
		[SerializeField] private MoneyViewer _moneyViewer;
		[SerializeField] private List<PooledParticleStruct> _particlePrefabs;
		[SerializeField] private List<VisitorCat> _visitorCatsPrefabs;

		public MainCat MainCatPrefab => _mainCatPrefab;

		public CatHelper CatHelper => _catHelper;

		public MoneyViewer MoneyViewer => _moneyViewer;

		public List<VisitorCat> VisitorCatsPrefabs => _visitorCatsPrefabs;

		public List<PooledParticleStruct> ParticlePrefabs => _particlePrefabs;
	}


	[Serializable]
	public struct PooledParticleStruct
	{
		public ParticleType ParticleType;
		public PooledParticle PooledParticle;
	}
}