﻿using UnityEditor;
using UnityEngine;
using static Common.Enums;

namespace Game.ScriptableObjects.Classes
{
	[CreateAssetMenu(fileName = "GardenBedData", menuName = "Garden/GardenBedData", order = 0)]
	public class GardenBedData : ScriptableObject
	{
		[SerializeField] private UpgradeType _productType;
		[SerializeField] private Sprite _sprite;
		[SerializeField] private float _processTime = 5;
		
		[Header("Sale")]
		[SerializeField] private ResourceType _resourceType;
		[SerializeField] private float _addedValue = 10f;
		

		public UpgradeType ProductType => _productType;
		
		public Sprite Sprite => _sprite;
		public float ProcessTime => _processTime;
		
		public ResourceType ResourceType => _resourceType;

		public float AddedValue => _addedValue;
		
	}
}