﻿using UnityEngine;

namespace Game.ScriptableObjects.Classes
{
	[CreateAssetMenu(fileName = "MainCatData", menuName = "Cats/MainCatData", order = 0)]
	public class MainCatData : ScriptableObject
	{
		[SerializeField] private float _processTime = 5;

		public float ProcessTime => _processTime;
	}
}