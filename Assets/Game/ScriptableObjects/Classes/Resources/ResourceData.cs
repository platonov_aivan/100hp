using UnityEditor;
using UnityEngine;
using static Common.Enums;

namespace ScriptableObjects.Classes.Resources
{
	[CreateAssetMenu(fileName = "New ResourceData", menuName = "Resource Data")]
	public class ResourceData : ScriptableObject
	{
		[SerializeField] private ResourceType _type;
		[SerializeField] private int _price;
		[SerializeField] private float _moveDeltaZ = 0.5f;
		[SerializeField] private float _moveZDuration = 1;

		[SerializeField] private Sprite _resourceIcon;
		[SerializeField] private Sprite _disabledResourceIcon;

		[SerializeField] private Mesh _resourceMesh;

		public Mesh ResourceMesh => _resourceMesh;
		public ResourceType Type => _type;
		public Sprite ResourceIcon => _resourceIcon;
		public int Price => _price;
		public Sprite DisabledResourceIcon => _disabledResourceIcon;

		public float MoveDeltaZ => _moveDeltaZ;

		public float MoveZDuration => _moveZDuration;


#if UNITY_EDITOR
		private void ApplyName()
		{
			var assetPath = AssetDatabase.GetAssetPath(this);
			AssetDatabase.RenameAsset(assetPath, _type.ToString());
			AssetDatabase.Refresh();
		}
#endif
	}
}