﻿using System;
using System.Collections.Generic;
using ScriptableObjects.Classes.Resources;
using UnityEditor;
using UnityEngine;
using static Common.Enums;

namespace ScriptableObjects.Classes.Upgrades
{
	[CreateAssetMenu(fileName = "New UpgradeData", menuName = "Upgrades/Upgrade Data")]
	public class UpgradeData : ScriptableObject
	{
		[SerializeField] private UpgradeType _type;
		[SerializeField] private string _text;

		[SerializeField]
		protected ResourceData _resource;

		[SerializeField] private List<UpgradeLevel> _levels;
		
		[SerializeField] private Sprite _image;

		public Sprite Image => _image;
		public string Text => _text;
		public UpgradeType Type => _type;
		public ResourceData Resource => _resource;
		public int MaxLevel => _levels.Count;
		public List<UpgradeLevel> Levels => _levels;
#if UNITY_EDITOR
		private void ApplyName()
		{
			var assetPath = AssetDatabase.GetAssetPath(this);
			AssetDatabase.RenameAsset(assetPath, _type.ToString());
			AssetDatabase.Refresh();
		}
#endif

		private void CheckLevels()
		{
			for (var i = 0; i < _levels.Count; i++)
			{
				UpdateLevelValue(i, l => l.Price, (l, v) => l.Price = (int) v);
				UpdateLevelValue(i, l => l.Value, (l, v) => l.Value = v);
			}
		}

		private void UpdateLevelValue(int index, Func<UpgradeLevel, float> getValue,
			Action<UpgradeLevel, float> setValue)
		{
			var level = _levels[index];
			if (getValue(_levels[index]) != 0) return;

			var prevLevel = index > 0 ? _levels[index - 1] : _levels[index];
			var prevPrevLevel = index > 1 ? _levels[index - 2] : prevLevel;
			var delta = getValue(prevLevel) - getValue(prevPrevLevel);
			setValue(level, getValue(prevLevel) + delta);
		}

		[Serializable]
		public class UpgradeLevel
		{
			[SerializeField] private int _price;
			[SerializeField] private float _value;

			public int Price
			{
				get => _price;
				set => _price = value;
			}

			public float Value
			{
				get => _value;
				set => _value = value;
			}
		}
	}
}