﻿using UnityEngine;

namespace Game.ScriptableObjects.Classes
{
	[CreateAssetMenu(fileName = "HelperData", menuName = "Cats/HelperData", order = 0)]
	public class HelperData : MainCatData
	{
	}
}