﻿using Common.ObjectPool;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace Game.Scripts.UI.Money
{
	public class MoneyViewer : PoolItem
	{
		[SerializeField] private CanvasGroup _canvasGroup;
		[SerializeField] private TextMeshProUGUI _countValueText;
		[SerializeField] private float _endValue = 0.5f;
		[SerializeField] private float _showDuration = 0.25f;
		[SerializeField] private float _endValueScale = 0.008f;

		private Sequence _sequence;


		public void Init(float value)
		{
			ShowValueText(value);
		}

		private void ShowValueText(float value)
		{
			_countValueText.text = $"+{value}";

			_sequence?.Kill();
			_sequence = DOTween.Sequence();

			_sequence.Append(_canvasGroup.DOFade(1, _showDuration)
				.SetLink(_canvasGroup.transform.gameObject));
			_sequence.Append(_canvasGroup.transform.DOLocalMoveY(_endValue, _showDuration * 3)
				.SetRelative()
				.SetLink(_canvasGroup.transform.gameObject));
			_sequence.Append(_canvasGroup.transform.DOScale(Vector3.zero, _showDuration)
				.SetLink(_canvasGroup.transform.gameObject));
			_sequence.Append(_canvasGroup.DOFade(0, _showDuration)
				.SetLink(_canvasGroup.transform.gameObject));
			_sequence.Append(_canvasGroup.transform.DOScale(_endValueScale, default)
				.SetLink(_canvasGroup.transform.gameObject));

			_sequence.AppendCallback(() => Release());
		}
	}
}