using System;
using Gameplay;
using ScriptableObjects.Classes.Upgrades;
using TMPro;
using UI.ResourcesView;
using UnityEngine;
using UnityEngine.UI;
using static Common.Enums;

namespace UI.Upgrades
{
	public class UpgradeElement : MonoBehaviour
	{
		[SerializeField] private TextMeshProUGUI _label;
		[SerializeField] private BuyButton       _button;
		[SerializeField] private Image           _icon;

		[SerializeField] private UpgradeType _type;

		private UpgradeData _data;

		private void Start()
		{
			_data = UpgradeProvider.GetUpgrade(_type);
			ResourceHandler.OnValueChanged += CheckButton;
			CheckButton();
		}

		private void OnDestroy()
		{
			ResourceHandler.OnValueChanged -= CheckButton;
		}

		private void OnEnable()
		{
			_button.OnClick += TryUpgrade;
		}

		private void OnDisable()
		{
			_button.OnClick -= TryUpgrade;
		}

		public void CheckButton()
		{
			CheckButton(_data.Resource.Type, ResourceHandler.GetResourceCount(_data.Resource.Type));
		}

		private void CheckButton(ResourceType resource, float count)
		{
			if (resource != _data.Resource.Type) return;

			var max    = UpgradeProvider.IsUpgradeMaxLevel(_type);
			var active = !max;
			var text   = "MAX";
			if (!max)
			{
				var cost = UpgradeProvider.GetCost(_type);
				text = cost.ToString();
				active = cost <= count;
			}

			_button.Init("UPGRADE", text, _data.Resource.ResourceIcon);
			_button.SetInteractable(active);
			
			_label.text = _data.Text;
			_icon.sprite = _data.Image;
		}

		private void TryUpgrade()
		{
			if (UpgradeProvider.IsUpgradeMaxLevel(_type)) return;

			var cost = UpgradeProvider.GetCost(_type);
			if (cost > ResourceHandler.GetResourceCount(_data.Resource.Type)) return;

			UpgradeProvider.IncreaseUpgradeLevel(_type);
			ResourceHandler.TrySubtractResource(_data.Resource.Type, cost, true);
		}
	}
}