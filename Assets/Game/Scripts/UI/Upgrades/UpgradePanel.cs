﻿using UI.Buttons;
using UnityEngine;

namespace UI.Upgrades
{
	public class UpgradePanel : MonoBehaviour
	{
		[SerializeField] private MyButton _shopButton;
		[SerializeField] private MyButton _backButton;
		[SerializeField] private MyButton _exitButton;
		[SerializeField] private CanvasGroup _upgradePanelCanvas;
		[SerializeField] private float _showDuration = 0.25f;
		
		private void Awake()
		{
			_shopButton.OnClick += OpenUpgrades;
			_backButton.OnClick += CloseUpgrades;
			_exitButton.OnClick += CloseUpgrades;
		}
		
		private void OnDestroy()
		{
			_shopButton.OnClick -= OpenUpgrades;
			_backButton.OnClick -= CloseUpgrades;
			_exitButton.OnClick -= CloseUpgrades;
		}
		
		private void OpenUpgrades()
		{
			_upgradePanelCanvas.Show(_showDuration);
		}

		private void CloseUpgrades()
		{
			_upgradePanelCanvas.Hide(_showDuration);
		}
	}
}