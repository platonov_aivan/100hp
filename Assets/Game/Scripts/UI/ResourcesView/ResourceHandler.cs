using System;
using System.Collections.Generic;
using Common;
using UnityEngine;
using static Common.Enums;

namespace UI.ResourcesView
{
	public static class ResourceHandler
	{
		public static event Action<ResourceType, float> OnValueSet;
		public static event Action<ResourceType, float, Vector3, bool> OnValueAdded;
		public static event Action<ResourceType, float> OnValueSubtracted;
		public static event Action<ResourceType, float> OnValueChanged;

		private static readonly Dictionary<ResourceType, FloatDataValueSavable> resources = new();

		public static bool AddResource(ResourceType type, float addedValue, bool autoSave = true,
			Vector3 screenPosition = default, bool fly = true)
		{
			if (addedValue < 0) return TrySubtractResource(type, -addedValue, autoSave);

			if (!resources.ContainsKey(type)) resources.Add(type, new FloatDataValueSavable(type.ToString()));

			OnValueAdded?.Invoke(type, addedValue, screenPosition, fly);
			var value = resources[type].Value += addedValue;
			OnValueChanged?.Invoke(type, value);
			if (autoSave) SaveData(type);
			return true;
		}

		public static Dictionary<ResourceType, FloatDataValueSavable> Resources => resources;

		public static Dictionary<ResourceType, FloatDataValueSavable> Resources1 => resources;

		public static bool TrySubtractResource(ResourceType type, float subtractValue, bool autoSave = true)
		{
			if (subtractValue < 0) throw new ArgumentOutOfRangeException(null, "Can't subtract negative value");

			if (!resources.ContainsKey(type)) resources.Add(type, new FloatDataValueSavable(type.ToString()));
			var value = resources[type].Value;
			if (value < subtractValue) throw new ArgumentOutOfRangeException(null, $"Not enough resource {type}");

			OnValueSubtracted?.Invoke(type, subtractValue);
			resources[type].Value = value -= subtractValue;
			OnValueChanged?.Invoke(type, value);
			if (autoSave) SaveData(type);
			return true;
		}

		public static bool ResetResource(ResourceType type, bool autoSave = true)
		{
			if (!resources.ContainsKey(type)) resources.Add(type, new FloatDataValueSavable(type.ToString()));
			resources[type].Value = 0;
			OnValueChanged?.Invoke(type, 0);
			OnValueSet?.Invoke(type, 0);
			if (autoSave) SaveData(type);
			return true;
		}

		public static void SaveData()
		{
			foreach (var valueSavable in resources)
			{
				valueSavable.Value.Save();
			}
		}

		public static void SaveData(ResourceType type)
		{
			if (resources.ContainsKey(type))
			{
				resources[type].Save();
			}
		}

		public static float GetResourceCount(ResourceType type)
		{
			if (!resources.ContainsKey(type)) resources.Add(type, new FloatDataValueSavable(type.ToString()));
			return resources[type].Value;
		}

		public static void ResetAllData(List<ResourceType> except = default)
		{
			except ??= new List<ResourceType> {ResourceType.Money};
			foreach (ResourceType resourceType in Enum.GetValues(typeof(ResourceType)))
			{
				if (except.Contains(resourceType)) continue;

				TrySubtractResource(resourceType, GetResourceCount(resourceType));
			}
		}

		public static void ResetData(List<ResourceType> currentTypes = default)
		{
			currentTypes ??= new List<ResourceType> {ResourceType.Money};
			foreach (ResourceType resourceType in Enum.GetValues(typeof(ResourceType)))
			{
				if (!currentTypes.Contains(resourceType)) continue;

				ResetResource(resourceType);
			}
		}

		public static void LoadAllData()
		{
			foreach (ResourceType resourceType in Enum.GetValues(typeof(ResourceType)))
			{
				LoadData(resourceType);
			}
		}

		private static void LoadData(ResourceType type)
		{
			var saveData = new FloatDataValueSavable(type.ToString());
			if (!resources.ContainsKey(type))
			{
				resources.Add(type, saveData);
			}
			else
			{
				resources[type].Value = saveData.Value;
			}

			OnValueSet?.Invoke(type, resources[type].Value);
		}
	}
}