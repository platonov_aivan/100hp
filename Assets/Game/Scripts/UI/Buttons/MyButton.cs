﻿using Common;
using DG.Tweening;
using Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Buttons
{
	[RequireComponent(typeof(Button), typeof(Image))]
	public class MyButton : MonoBehaviour
	{
		public event System.Action OnClick;

		[SerializeField] private float _scalerDuration = 0.25f;

		private Button   _button;
		protected bool     _interactable = true;
		protected Image    _image;
		private Sequence _sequence;

		public void Show(bool immediate = false) => Toggle(true, immediate);
		public void Hide(bool immediate = false) => Toggle(false, immediate);

		public virtual void SetInteractable(bool value)
		{
			_image ??= GetComponent<Image>();

			_interactable = value;
		}

		protected virtual void Awake()
		{
			_image = GetComponent<Image>();
			_button = GetComponent<Button>();

			_button.onClick.AddListener(ClickButton);
		}

		protected virtual void ClickButton()
		{
			if (!_interactable) return;

			OnClick?.Invoke();
			MyVibration.Haptic(MyHapticTypes.LightImpact);
		}

		private void Toggle(bool value, bool immediate)
		{
			_sequence?.Kill();
			SetInteractable(value);
			if (immediate)
			{
				transform.localScale = value ? Vector3.one : Vector3.zero;
			}
			else
			{
				_sequence = DOTween.Sequence();
				_sequence.Append(transform.DOScale(1.25f, _scalerDuration));
				_sequence.Append(transform.DOScale(value ? 1f : 0f, _scalerDuration));
			}
		}
	}
}