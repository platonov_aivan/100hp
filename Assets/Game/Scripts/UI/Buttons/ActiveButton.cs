using Extensions;
using UI.Buttons;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
	public class ActiveButton : MyButton
	{
		[SerializeField] private Image     _buttonBack;
		[SerializeField] private Sprite _activeSprite;
		[SerializeField] private Sprite _inactiveSprite;

		public override void SetInteractable(bool value)
		{
			base.SetInteractable(value);
			_buttonBack.sprite = value ? _activeSprite : _inactiveSprite;
		}
		
	}
}