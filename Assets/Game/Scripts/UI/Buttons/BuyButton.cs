using Extensions;
using TMPro;
using UI.Buttons;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
	public class BuyButton : ActiveButton
	{
		[SerializeField] private TextMeshProUGUI _header;
		[SerializeField] private TextMeshProUGUI _price;
		[SerializeField] private Image _resourceIcon;

		public void Init(string header, string price, Sprite icon = null)
		{
			if (_header != default) _header.text = header;
			if (_price != default) _price.text = price;
			if (icon != default && _resourceIcon != default) _resourceIcon.sprite = icon;
		}
	}
}