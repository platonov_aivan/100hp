﻿using UnityEngine;
using Utils;

namespace Gameplay
{
    public class InputHandler : MonoBehaviour
    {
        [SerializeField] protected LayerMask Layers;
        
        protected Vector3 StartTouchPosition;
        protected Vector3 CurrentTouchPosition;
        
        public bool IsActive { get; set; }
        private bool _isClicked;

        private void Update()
        {
            if (!IsActive) return;
#if UNITY_EDITOR
            HandleMouse();
#elif UNITY_ANDROID || UNITY_IOS
            HandleTouch();
#endif
        }

        private void HandleMouse()
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (!Helper.IsPointerOverUI())
                {
                    _isClicked = true;
                    MouseDown();
                }
            }
            
            if (Input.GetMouseButton(0))
            {
                if (_isClicked)
                    MouseHold();
            }
            
            if (Input.GetMouseButtonUp(0))
            {
                _isClicked = false;
                MouseUp();
            }
        }
        
        private void HandleTouch()
        {
            if (Input.touchCount == 0) return;
            if (Input.touchCount > 1)
            {
	            _isClicked = false;
	            MouseUp();
	            return;
            }
            var touch = Input.GetTouch(0);
            
            if (touch.phase == TouchPhase.Began)
            {
                if (Helper.IsPointerOverUI()) return;
                _isClicked = true;
                MouseDown();
            }
            
            if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
            {
                if (_isClicked)
                    MouseHold();
            }
            
            if (touch.phase == TouchPhase.Ended)
            {
                _isClicked = false;
                MouseUp();
            }
        }
        
        protected virtual void MouseDown()
        {
            StartTouchPosition = Input.mousePosition;
        }

        protected virtual void MouseHold()
        {
            CurrentTouchPosition = Input.mousePosition;
        }

        protected virtual void MouseUp()
        {
        }
    }
}