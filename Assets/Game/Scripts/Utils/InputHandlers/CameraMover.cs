﻿using Cinemachine;
using Common.Providers;
using DG.Tweening;
using Extensions;
using ScriptableObjects.Classes.Prefabs;
using UnityEngine;
using Utils;

namespace Gameplay
{
	public class CameraMover : InputHandler
	{
		[SerializeField] private CinemachineVirtualCamera _freeLookVirtualCamera;
		[SerializeField] private GameData _gameData;

		[SerializeField, Min(0f)] private float _sensitivity = 1f;

		[SerializeField] private bool _isMoveAxisX;
		[SerializeField] private float _minXBorder;
		[SerializeField] private float _maxXBorder;
		[SerializeField] private bool _isMoveAxisY;
		[SerializeField] private float _minYBorder;
		[SerializeField] private float _maxYBorder;
		[SerializeField] private bool _showGizmo;

		private RaycastHit[] _hits = new RaycastHit[1];
		private Vector3 _cameraPosition;
		private bool _unitHitted;
		private bool _isCameraMoving;

		private void Start()
		{
			IsActive = true;
		}

		protected override void MouseDown()
		{
			base.MouseDown();

			/*var hitCount = Physics.RaycastNonAlloc(Helper.Camera.ScreenPointToRay(Input.mousePosition), _hits, 300f, Layers);
			for (int i = 0; i < hitCount; i++)
			{
			   if (_hits[i].collider.TryGetComponent(out Unit _))
			    {
			        _unitHitted = true;
			        return;
			    }
			}*/

			_cameraPosition = Helper.Camera.transform.position;
			StopAllCoroutines();
		}

		protected override void MouseHold()
		{
			base.MouseHold();

			if (_unitHitted) return;

			if (!_isCameraMoving && StartTouchPosition.SqrDistanceTo(CurrentTouchPosition) <
			    Mathf.Pow(_gameData.TouchDistanceToHoldObject, 2))
				return;

			DragCamera();
		}

		protected override void MouseUp()
		{
			base.MouseUp();
			_unitHitted = false;
			_isCameraMoving = false;
		}

		private void DragCamera()
		{
			if (!_isCameraMoving)
			{
				_isCameraMoving = true;
				return;
			}

			CurrentTouchPosition.z = StartTouchPosition.z = _cameraPosition.y;

			Vector3 direction = Helper.Camera.ScreenToWorldPoint(CurrentTouchPosition) -
			                    Helper.Camera.ScreenToWorldPoint(StartTouchPosition);

			direction = direction * -1 * _sensitivity;

			Vector3 position = _cameraPosition + direction;
			position = new Vector3(
				_isMoveAxisX
					? Mathf.Clamp(position.x, _minXBorder, _maxXBorder)
					: _cameraPosition.x,
				_cameraPosition.y,
				_cameraPosition.z);


			CameraProvider.PlayerCamera.transform.DOMove(position, 0.1f);
		}

		private void OnDrawGizmos()
		{
			if (!_showGizmo) return;
		}
	}
}