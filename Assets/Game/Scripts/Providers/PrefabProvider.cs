﻿using System;
using System.Linq;
using Common.ObjectPool;
using Extensions;
using Game.Scripts.Commons.ObjectPool;
using Game.Scripts.Gameplay.Сats.MainCat;
using Game.Scripts.Gameplay.Сats.MainCat.Helper;
using Game.Scripts.Gameplay.Сats.Queue.VisitorCats;
using Game.Scripts.UI.Money;
using ScriptableObjects.Classes.Prefabs;
using UnityEngine;
using Utils;
using static Common.Enums;

namespace Game.Scripts.Providers
{
	public class PrefabProvider : Singleton<PrefabProvider>
	{
		[SerializeField] private PrefabData _prefabData;

		public static PooledParticle GetParticlePrefab(ParticleType type)
		{
			if (Instance._prefabData.ParticlePrefabs.All(x => x.ParticleType != type))
			{
				throw new NullReferenceException($"Check Particle Prefab Data {type}");
			}

			return Instance._prefabData.ParticlePrefabs.FirstOrDefault(x => x.ParticleType == type).PooledParticle;
		}

		public static MainCat GetCatPrefab()
		{
			if (!Instance._prefabData.MainCatPrefab)
			{
				throw new NullReferenceException($"Check Particle Prefab Data MainCat");
			}

			return Instance._prefabData.MainCatPrefab;
		}	
		public static CatHelper GetCatHelper()
		{
			if (!Instance._prefabData.CatHelper)
			{
				throw new NullReferenceException($"Check Particle Prefab Data MainCat");
			}

			return Instance._prefabData.CatHelper;
		}	
		public static MoneyViewer GetMoneyViewer()
		{
			if (!Instance._prefabData.MoneyViewer)
			{
				throw new NullReferenceException($"Check Particle Prefab Data MainCat");
			}

			return Instance._prefabData.MoneyViewer;
		}

		public static VisitorCat GetVisitorCat()
		{
			if (Instance._prefabData.VisitorCatsPrefabs.Count <= 0)
			{
				throw new NullReferenceException($"Check Particle Prefab Data VisitorCat");
			}

			return Instance._prefabData.VisitorCatsPrefabs.GetRandomElement();
		}
	}
}