using System.Collections.Generic;
using Cinemachine;
using DG.Tweening;
using Extensions;
using Gameplay;
using UnityEngine;
using Utils;

namespace Common.Providers
{
    public class CameraProvider : Singleton<CameraProvider>
    {
        private static CinemachineBasicMultiChannelPerlin _playerNoise;
        private static Tweener _shakeTween;
        
        [SerializeField] private Camera _mainCamera;
        [SerializeField]  private List<CinemachineVirtualCamera> _targetCameras;
        [SerializeField]  private CinemachineVirtualCamera _playerCamera;
        [SerializeField]  private CinemachineBrain _brain;

        [SerializeField]  private float _shakeAmplitude;
        [SerializeField]  private float _shakeFrequency;
        [SerializeField]  private float _shakeDuration;

        private static int _targetCameraIndex;
        private Vector3 _defaultCameraPos;
        private static List<CinemachineVirtualCamera> TargetCameras => Instance._targetCameras;
        public static CinemachineVirtualCamera PlayerCamera => Instance._playerCamera;
        private static CinemachineBrain Brain => Instance._brain;
        private static Camera MainCamera => Instance._mainCamera;

        protected override void Awake()
        {
            base.Awake();
            _playerNoise = PlayerCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();

            _defaultCameraPos = PlayerCamera.transform.position;
        }

       

        public static void SetBlendTime(float time)
        {
            Brain.m_DefaultBlend.m_Time = time;
        }

        public static void SetTarget(Transform target)
        {
            _targetCameraIndex += 1;
            if (_targetCameraIndex == TargetCameras.Count)
                _targetCameraIndex = 0;
            
            SetupCamera(TargetCameras[_targetCameraIndex], target);
        }

        public static void SetPlayerTarget(Transform target)
        {
            SetupCamera(PlayerCamera, target);
        }
        
        public static void ShowPlayer()
        {
            DisableAllCameras();
            PlayerCamera.Priority = 1;
        }

        public static void ShowTarget()
        {
            DisableAllCameras();
            TargetCameras[_targetCameraIndex].Priority = 1;
        }
        
        public static void Shake(float amplitude = 0, float frequency = 0, float duration = 0)
        {
            if (amplitude == 0) amplitude = Instance._shakeAmplitude;
            if (frequency == 0) frequency = Instance._shakeFrequency;
            if (duration == 0) duration = Instance._shakeDuration;

            _playerNoise.m_FrequencyGain = frequency;
            _shakeTween?.Kill();
            _shakeTween = DOTween.To(x => _playerNoise.m_AmplitudeGain = x, 0, amplitude, duration / 2)
                .OnComplete(() => DOTween.To(x => _playerNoise.m_AmplitudeGain = x, amplitude, 0,
                    duration / 2));
        }
        
        
        private static void SetupCamera(CinemachineVirtualCamera virtualCamera, Transform target)
        {
            if (virtualCamera == default)
                return;

            virtualCamera.Follow = target;
            virtualCamera.LookAt = target;
        }

        private static void DisableAllCameras()
        { 
            PlayerCamera.Priority = 0;
            
            foreach (var camera in TargetCameras)
                camera.Priority = 0;
        }
    }
}