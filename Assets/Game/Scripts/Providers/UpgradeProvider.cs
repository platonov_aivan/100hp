﻿using System;
using System.Collections.Generic;
using Common;
using ScriptableObjects.Classes.Upgrades;
using UnityEngine;
using Utils;
using static Common.Constants.SavePrefix;
using static Common.Enums;

namespace Gameplay
{
	public class UpgradeProvider : Singleton<UpgradeProvider>
	{
		public static event Action<UpgradeType> OnUpgrade;

		[SerializeField] private List<UpgradeData> _upgradeData;

		private static readonly Dictionary<UpgradeType, int> upgradesLevel = new();
		private static readonly Dictionary<UpgradeType, UpgradeData> upgradesDataDict = new();

		protected override void Awake()
		{
			base.Awake();

			foreach (var upgradeData in _upgradeData)
			{
				upgradesDataDict[upgradeData.Type] = upgradeData;
			}

			LoadPrefs();
		}

		private void LoadPrefs()
		{
			foreach (var upgradeData in _upgradeData)
			{
				upgradesLevel[upgradeData.Type] =
					new IntDataValueSavable(GetSaveUpgradeDataName(upgradeData.Type)).Value;
			}
		}

		public static int GetCost(UpgradeType upgradeType) =>
			upgradesDataDict[upgradeType].Levels[upgradesLevel[upgradeType]].Price;

		public static float GetUpgradeValue(UpgradeType upgradeType) =>
			upgradesDataDict[upgradeType].Levels[upgradesLevel[upgradeType]].Value;

		public static UpgradeData GetUpgrade(UpgradeType upgradeType) => upgradesDataDict[upgradeType];

		public static int GetUpgradeLevel(UpgradeType upgradeType) => upgradesLevel[upgradeType] ;

		public static bool IsUpgradeMaxLevel(UpgradeType upgradeType) =>
			upgradesLevel[upgradeType] + 1 >= upgradesDataDict[upgradeType].MaxLevel;

		public static void IncreaseUpgradeLevel(UpgradeType upgradeType)
		{
			SaveUpgradeLevel(upgradeType, ++upgradesLevel[upgradeType]);
			OnUpgrade?.Invoke(upgradeType);
			Debug.Log(
				$"Upgrade {upgradeType} to level {upgradesLevel[upgradeType]}. Value: {GetUpgradeValue(upgradeType)}");
		}

		public static void SaveUpgradesLevel()
		{
			foreach (var (type, level) in upgradesLevel)
			{
				SaveUpgradeLevel(type, level);
			}
		}

		private static void SaveUpgradeLevel(UpgradeType upgradeType, int level)
		{
			var upgradeData = new IntDataValueSavable(GetSaveUpgradeDataName(upgradeType));
			upgradeData.Value = level;
			upgradeData.Save();
		}

		private static string GetSaveUpgradeDataName(UpgradeType upgradeType) => $"{UPGRADE}{upgradeType}";
	}
}