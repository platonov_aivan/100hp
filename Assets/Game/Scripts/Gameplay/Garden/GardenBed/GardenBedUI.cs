﻿using DG.Tweening;
using UI.Buttons;
using UI.ProgressBars;
using UnityEngine;

namespace Game.Scripts.Gameplay.Garden.GardenBed
{
	public class GardenBedUI : MonoBehaviour
	{
		[Header("Components")]
		[SerializeField] private ProgressBar _progressBar;
		[SerializeField] private float _scaleFactor = 1.2f;
		[SerializeField] private float _scaleDuration = 1f;
		[Space]
		[Header("Canvas")]
		[SerializeField] private CanvasGroup _canvasProgressBar;
		[SerializeField] private CanvasGroup _canvasHarvestingBar;
		[SerializeField] private float _showDuration = 0.25f;
		[Space]
		[Header("HarvestingButton")]
		[SerializeField] private Transform _backReward;
		[SerializeField] private MyButton _harvestingButton;
		[SerializeField] private float _scalerDuration = 0.25f;

		public MyButton HarvestingButton => _harvestingButton;


		private Sequence _sequence;


		public void ShowProgressBar() => ShowProgressBar(true);

		public void HideProgressBar() => ShowProgressBar(false);

		public void ShowHarvesting() => ShowHarvesting(true);

		public void HideHarvesting() => ShowHarvesting(false);
		public void HideRewardButton() => EnableRewardButton(false);

		public void UpdateProgress(float currentProgress)
		{
			_progressBar.SetValue(currentProgress);
		}

		public void BounceBar(bool activate)
		{
			_progressBar.transform.DOKill();

			if (activate)
			{
				_progressBar.transform.DOScale(Vector3.one * _scaleFactor, _scaleDuration)
					.SetLink(_progressBar.transform.gameObject)
					.SetLoops(-1, LoopType.Yoyo);
			}
			else
			{
				_progressBar.transform.DOScale(Vector3.one, default)
					.SetLink(_progressBar.transform.gameObject);
			}
		}

		private void ShowProgressBar(bool activate)
		{
			if (activate)
			{
				_canvasProgressBar.Show(_showDuration);
			}
			else
			{
				_canvasProgressBar.Hide(_showDuration);
			}
		}

		private void ShowHarvesting(bool activate)
		{
			if (activate)
			{
				_canvasHarvestingBar.Show(_showDuration, callback: ShowRewardButton);
			}
			else
			{
				_canvasHarvestingBar.Hide(_showDuration);
			}
		}

		private void ShowRewardButton() => EnableRewardButton(true);

		private void EnableRewardButton(bool enable)
		{
			_harvestingButton.SetInteractable(enable);
			_sequence?.Kill();
			_sequence = DOTween.Sequence();
			_sequence.Append(_backReward.DOScale(1.25f, _scalerDuration)).SetLink(gameObject);
			_sequence.Append(_backReward.DOScale(enable ? 1f : 0f, _scalerDuration)).SetLink(gameObject);
		}
	}
}