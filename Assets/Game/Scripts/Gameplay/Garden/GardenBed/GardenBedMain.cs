﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Extensions;
using Game.ScriptableObjects.Classes;
using Game.Scripts.Gameplay.Сats.Queue.VisitorCats;
using Gameplay;
using UnityEngine;
using static Common.Enums;

namespace Game.Scripts.Gameplay.Garden.GardenBed
{
	public class GardenBedMain : MonoBehaviour
	{
		public static event Action<Transform, GardenBedMain, VisitorCat> OnHarvesting;

		[SerializeField] private GardenBedData _data;
		[SerializeField] private GardenBedUI _gardenBedUI;
		[SerializeField] private Transform _harvestingPoint;
		public bool IsActivate => _isActivate;
		public GardenBedData Data => _data;

		public GardenBedUI GardenBedUI => _gardenBedUI;

		private readonly List<VisitorCat> _willingVisitors = new();

		private bool _isActivate;
		private bool _isReadyToHarvest;

		private float _currentProgress;
		private Coroutine _processCor;
		private VisitorCat _currentVisitor;


		private void Awake()
		{
			_isActivate = true;
			
			VisitorCat.OnProductActivate += CheckForHarvestProduct;
			_gardenBedUI.HarvestingButton.OnClick += Harvesting;
		}

		private void Start()
		{
			StartProcess();
		}

		private void OnDestroy()
		{
			VisitorCat.OnProductActivate -= CheckForHarvestProduct;
			_gardenBedUI.HarvestingButton.OnClick -= Harvesting;
		}

		public void StartProcess()
		{
			_processCor.Stop(this);
			_processCor = StartCoroutine(ProcessCor());
		}

		private void CheckForHarvestProduct(UpgradeType productType, VisitorCat visitorCat)
		{
			if (_data.ProductType != productType) return;
			_isReadyToHarvest = true;

			if (!_willingVisitors.Contains(visitorCat))
			{
				_willingVisitors.Add(visitorCat);
			}
		}

		private void Harvesting()
		{
			if (!_isReadyToHarvest) return;
			
			var visitor = _willingVisitors.FirstOrDefault();

			if (visitor == default) return;

			OnHarvesting?.Invoke(_harvestingPoint, this, visitor);
			
		}

		public void HarvestingProcess(VisitorCat visitor)
		{
			if (_willingVisitors.Contains(visitor))
			{
				_willingVisitors.Remove(visitor);
			}

			if (_willingVisitors.Count <= 0)
			{
				_isReadyToHarvest = false;
			}

			_gardenBedUI.HideRewardButton();
		}

		private IEnumerator ProcessCor()
		{
			_gardenBedUI.ShowProgressBar();
			
			_gardenBedUI.BounceBar(true);

			var timer = Time.time;

			var dataProcessTime = _data.ProcessTime - UpgradeProvider.GetUpgradeValue(UpgradeType.ImprovementGardenBed);
			
			while (timer + dataProcessTime >= Time.time)
			{
				yield return null;
				_currentProgress += Time.deltaTime;

				var currentTime = _currentProgress / dataProcessTime;
				_gardenBedUI.UpdateProgress(currentTime);
			}

			_currentProgress = 0f;
			_gardenBedUI.UpdateProgress(_currentProgress);
			
			ProcessDone();
		}

		private void ProcessDone()
		{
			_gardenBedUI.BounceBar(false);
			
			_gardenBedUI.HideProgressBar();
			_gardenBedUI.ShowHarvesting();
		}
	}
}