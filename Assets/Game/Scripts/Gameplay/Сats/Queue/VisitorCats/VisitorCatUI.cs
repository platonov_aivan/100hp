﻿using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.Gameplay.Сats.Queue.VisitorCats
{
	public class VisitorCatUI : CatBaseUI
	{
		[SerializeField] private Image _iconProduct;
		[Space]
		[SerializeField] private CanvasGroup _waitProductCanvas;
		[SerializeField] private float _showDuration = 0.25f;
		

		public void ShowProductBar(Sprite sprite)
		{
			_iconProduct.sprite = sprite;

			_waitProductCanvas.Show(_showDuration);
		}

		public void HideProductBar() => _waitProductCanvas.Hide(_showDuration);
		
	}
}