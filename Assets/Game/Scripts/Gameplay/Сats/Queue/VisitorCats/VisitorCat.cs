﻿using System;
using Common.ObjectPool;
using Game.ScriptableObjects.Classes;
using Game.Scripts.Gameplay.Reception;
using Game.Scripts.Providers;
using Gameplay;
using UI.ResourcesView;
using UnityEngine;
using static Common.Enums;

namespace Game.Scripts.Gameplay.Сats.Queue.VisitorCats
{
	public class VisitorCat : CatBase
	{
		public static event Action<VisitorCat> OnProductRequest;
		public static event Action<UpgradeType, VisitorCat> OnProductActivate;

		[Header("Components")]
		[SerializeField] private VisitorCatUI _visitorCatUI;
		[SerializeField] private Transform _moneyViewerPos;
		[SerializeField] private VisitorCatData _data;


		public ReceptionPoint ReceptionPoint => _receptionPoint;

		private ReceptionPoint _receptionPoint;
		private Transform _removePoint;


		public void Init(ReceptionPoint receptionPoint, Transform removePoint)
		{
			_receptionPoint = receptionPoint;
			_removePoint = removePoint;


			MoveTo(_receptionPoint.PointTransform, ProductRequest);
		}

		public void SetProduct(GardenBedData gardenBedData)
		{
			_visitorCatUI.ShowProductBar(gardenBedData.Sprite);

			OnProductActivate?.Invoke(gardenBedData.ProductType, this);
		}

		public void SaleDone(GardenBedData gardenBedData)
		{
			_receptionPoint.IsBusy = false;

			var dataAddedValue = gardenBedData.AddedValue + UpgradeProvider.GetUpgradeValue(gardenBedData.ProductType);

			ResourceHandler.AddResource(gardenBedData.ResourceType, dataAddedValue);
			var moneyViewer = Pool.Get(PrefabProvider.GetMoneyViewer(), _moneyViewerPos.position);
			moneyViewer.Init(dataAddedValue);

			_visitorCatUI.HideProductBar();

			MoveTo(_removePoint, RemoveVisitor);
		}

		private void RemoveVisitor()
		{
			Release();
		}


		private void ProductRequest()
		{
			OnProductRequest?.Invoke(this);
		}
	}
}