﻿using System.Collections;
using System.Collections.Generic;
using Common.ObjectPool;
using Extensions;
using Game.Scripts.Gameplay.Reception;
using Game.Scripts.Providers;
using ScriptableObjects.Classes.Prefabs;
using UnityEngine;

namespace Game.Scripts.Gameplay.Сats.Queue
{
	public class QueueCats : MonoBehaviour
	{
		[SerializeField] private float _spawnDuration = 10f;

		private Coroutine _queueCor;
		private List<ReceptionPoint> _receptionPoints;

		private Transform _spawnPoint;
		private Transform _removePoint;

		public void Init(Transform spawnPoint, Transform removePoint, List<ReceptionPoint> receptionPoint)
		{
			_spawnPoint = spawnPoint;
			_removePoint = removePoint;
			_receptionPoints = receptionPoint;

			_queueCor.Stop(this);
			_queueCor = StartCoroutine(QueueCor());
		}

		private IEnumerator QueueCor()
		{
			while (true)
			{
				SpawnVisitor();
				yield return new WaitForSeconds(_spawnDuration);
			}
		}

		private void SpawnVisitor()
		{
			foreach (var receptionPoint in _receptionPoints)
			{
				if (receptionPoint.IsBusy) continue;
				receptionPoint.IsBusy = true;
				var visitorCat = Pool.Get(PrefabProvider.GetVisitorCat(), _spawnPoint.position);
				visitorCat.Init(receptionPoint, _removePoint);
				break;
			}
		}
	}
}