﻿using UI.ProgressBars;
using UnityEngine;

namespace Game.Scripts.Gameplay.Сats
{
	public class CatBaseUI : MonoBehaviour
	{
		[SerializeField] private ProgressBar _progressBar;


		public void UpdateProgress(float currentProgress)
		{
			_progressBar.SetValue(currentProgress);
		}
	}
}