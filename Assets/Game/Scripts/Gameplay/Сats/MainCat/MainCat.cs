﻿using System;
using System.Collections;
using System.Collections.Generic;
using Extensions;
using Game.ScriptableObjects.Classes;
using Game.Scripts.Gameplay.Garden.GardenBed;
using Game.Scripts.Gameplay.Сats.MainCat.Helper;
using Game.Scripts.Gameplay.Сats.Queue.VisitorCats;
using UnityEngine;

namespace Game.Scripts.Gameplay.Сats.MainCat
{
	public class MainCat : CatBase
	{
		[Header("Components")]
		[SerializeField] private MainCatUI _mainCatUI;
		[Space]
		[Header("Data")]
		[SerializeField] private MainCatData _data;

		private Coroutine _processCor;
		private float _currentProgress;

		private bool _isCatInProcess;
		private GardenBedMain _currentGardenBed;
		private VisitorCat _currentVisitor;

		private readonly List<CatHelper> _catHelpers = new();

		public void AddHelpers(CatHelper catHelper)
		{
			if (_catHelpers.Contains(catHelper)) return;
			_catHelpers.Add(catHelper);

			Debug.Log(catHelper.name);
		}

		private void Awake()
		{
			GardenBedMain.OnHarvesting += StartHarvestCat;
		}

		private void OnDestroy()
		{
			GardenBedMain.OnHarvesting -= StartHarvestCat;
		}

		private void StartHarvestCat(Transform harvestingPoint, GardenBedMain gardenBed, VisitorCat visitorCat)
		{
			if (_isCatInProcess)
			{
				AssigningHelper(harvestingPoint, gardenBed, visitorCat);
				return;
			}

			_currentGardenBed = gardenBed;
			_currentVisitor = visitorCat;

			_isCatInProcess = true;
			gardenBed.HarvestingProcess(visitorCat);
			
			MoveTo(harvestingPoint, ProcessHarvesting);
		}

		private void AssigningHelper(Transform harvestingPoint, GardenBedMain gardenBed, VisitorCat visitorCat)
		{
			if (_catHelpers.Count <= 0) return;

			foreach (var catHelper in _catHelpers)
			{
				if (catHelper.IsCatInProcess) continue;
				catHelper.StartHarvestCat(harvestingPoint, gardenBed, visitorCat);
				break;
			}
		}


		private void ProcessHarvesting()
		{
			_processCor.Stop(this);
			_processCor = StartCoroutine(ProcessCor(true));
		}

		private void ProcessSale()
		{
			_processCor.Stop(this);
			_processCor = StartCoroutine(ProcessCor(false));
		}

		private IEnumerator ProcessCor(bool harvesting)
		{
			_mainCatUI.ActivateProcessCanvas(true);
			yield return new WaitForSeconds(_mainCatUI.ShowDuration);

			var timer = Time.time;

			while (timer + _data.ProcessTime >= Time.time)
			{
				yield return null;
				_currentProgress += Time.deltaTime;

				var currentTime = _currentProgress / _data.ProcessTime;
				_mainCatUI.UpdateProgress(currentTime);
			}

			_currentProgress = 0f;
			_mainCatUI.UpdateProgress(_currentProgress);
			_mainCatUI.ActivateProcessCanvas(false);

			if (harvesting)
			{
				HarvestingDone();
			}
			else
			{
				SaleDone();
			}
		}

		private void HarvestingDone()
		{
			_currentGardenBed.GardenBedUI.HideHarvesting();
			_currentGardenBed.StartProcess();

			MoveTo(_currentVisitor.ReceptionPoint.MainCatPointTransform, ProcessSale);
		}

		private void SaleDone()
		{
			_currentVisitor.SaleDone(_currentGardenBed.Data);

			_currentGardenBed = default;
			_currentVisitor = default;
			_isCatInProcess = false;
		}
	}
}