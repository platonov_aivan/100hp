﻿using UnityEngine;

namespace Game.Scripts.Gameplay.Сats.MainCat
{
	public class MainCatUI : CatBaseUI
	{
		[Space]
		[SerializeField] private CanvasGroup _processCanvas;
		[SerializeField] private float _showDuration = 0.25f;

		public float ShowDuration => _showDuration;

		public void ActivateProcessCanvas(bool activate)
		{
			if (activate)
			{
				_processCanvas.Show(_showDuration);
			}
			else
			{
				_processCanvas.Hide();
			}
		}
	}
}