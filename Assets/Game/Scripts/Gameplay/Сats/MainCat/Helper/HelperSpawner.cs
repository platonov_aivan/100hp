﻿using Common.ObjectPool;
using Extensions;
using Game.Scripts.Providers;
using Gameplay;
using UnityEngine;
using static Common.Enums;

namespace Game.Scripts.Gameplay.Сats.MainCat.Helper
{
	public class HelperSpawner : MonoBehaviour
	{
		[SerializeField] private Transform _helperSpawnPos;
		[SerializeField] private UpgradeType _type;
		[Space]
		[Header("MainCat")]
		[SerializeField] private MainCat _mainCat;

		private void Start()
		{
			UpgradeProvider.OnUpgrade += BuyHelper;

			LoadData();
		}

		private void OnDestroy()
		{
			UpgradeProvider.OnUpgrade -= BuyHelper;
		}

		private void BuyHelper(UpgradeType type)
		{
			if (_type == type)
			{
				LoadData();
			}
		}

		private void LoadData()
		{
			var countHelper = (int) UpgradeProvider.GetUpgradeValue(_type);

			for (var i = 1; i <= countHelper; i++)
			{
				var catHelper = Pool.Get(PrefabProvider.GetCatHelper(), _helperSpawnPos.position.XYOnly());
				_mainCat.AddHelpers(catHelper);
			}
		}
	}
}