﻿using System.Collections;
using Extensions;
using Game.ScriptableObjects.Classes;
using Game.Scripts.Gameplay.Garden.GardenBed;
using Game.Scripts.Gameplay.Сats.Queue.VisitorCats;
using UnityEngine;

namespace Game.Scripts.Gameplay.Сats.MainCat.Helper
{
	public class CatHelper : CatBase
	{
		[Header("Components")]
		[SerializeField] private MainCatUI _mainCatUI;
		[Space]
		[Header("Data")]
		[SerializeField] private HelperData _data;
		public bool IsCatInProcess => _isCatInProcess;

		private Coroutine _processCor;
		private float _currentProgress;

		private bool _isCatInProcess;
		private GardenBedMain _currentGardenBed;
		private VisitorCat _currentVisitor;


		public void StartHarvestCat(Transform harvestingPoint, GardenBedMain gardenBed, VisitorCat visitorCat)
		{
			if (_isCatInProcess)
			{
				return;
			}

			_currentGardenBed = gardenBed;
			_currentVisitor = visitorCat;

			_isCatInProcess = true;
			gardenBed.HarvestingProcess(visitorCat);

			MoveTo(harvestingPoint, ProcessHarvesting);
		}

		private void ProcessHarvesting()
		{
			_processCor.Stop(this);
			_processCor = StartCoroutine(ProcessCor(true));
		}

		private void ProcessSale()
		{
			_processCor.Stop(this);
			_processCor = StartCoroutine(ProcessCor(false));
		}

		private IEnumerator ProcessCor(bool harvesting)
		{
			_mainCatUI.ActivateProcessCanvas(true);
			yield return new WaitForSeconds(_mainCatUI.ShowDuration);

			var timer = Time.time;

			while (timer + _data.ProcessTime >= Time.time)
			{
				yield return null;
				_currentProgress += Time.deltaTime;

				var currentTime = _currentProgress / _data.ProcessTime;
				_mainCatUI.UpdateProgress(currentTime);
			}

			_currentProgress = 0f;
			_mainCatUI.UpdateProgress(_currentProgress);
			_mainCatUI.ActivateProcessCanvas(false);

			if (harvesting)
			{
				HarvestingDone();
			}
			else
			{
				SaleDone();
			}
		}

		private void HarvestingDone()
		{
			_currentGardenBed.GardenBedUI.HideHarvesting();
			_currentGardenBed.StartProcess();

			MoveTo(_currentVisitor.ReceptionPoint.MainCatPointTransform, ProcessSale);
		}

		private void SaleDone()
		{
			_currentVisitor.SaleDone(_currentGardenBed.Data);

			_currentGardenBed = default;
			_currentVisitor = default;
			_isCatInProcess = false;
		}
	}
}