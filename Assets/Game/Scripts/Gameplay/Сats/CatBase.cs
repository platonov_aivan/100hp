﻿using System;
using Common.ObjectPool;
using DG.Tweening;
using Extensions;
using UnityEngine;

namespace Game.Scripts.Gameplay.Сats
{
	public class CatBase : PoolItem
	{
		[Header("Components")]
		[SerializeField] protected Transform _mainCatTransform;
		[SerializeField] protected Transform _modelTransform;
		[Header("Settings")]
		[SerializeField] protected float _moveDuration;
		[SerializeField] private Ease _ease = Ease.Linear;

		[SerializeField] private float _rotateZ = 5f;
		[SerializeField] private float _rotateDuration = 0.2f;

		private Sequence _sequence;


		protected virtual void MoveTo(Transform point, Action action)
		{
			_modelTransform.DOKill();
			_modelTransform.DOScaleY(0.8f, _rotateDuration/2f).SetLoops(-1, LoopType.Yoyo).SetLink(_modelTransform.gameObject);
			_modelTransform.DOScaleX(1.1f, _rotateDuration/2f).SetLoops(-1, LoopType.Yoyo).SetLink(_modelTransform.gameObject);
			
			_sequence?.Kill();
			_sequence = DOTween.Sequence();
			_sequence.Append(_modelTransform.DOLocalRotate(Vector3.forward * _rotateZ, _rotateDuration)).SetLink(_modelTransform.gameObject);
			_sequence.Append(_modelTransform.DOLocalRotate(Vector3.forward * -_rotateZ, _rotateDuration)).SetLink(_modelTransform.gameObject);
			_sequence.SetLoops(-1, LoopType.Yoyo);
			
			
			
			
			_mainCatTransform.DOKill();
			_mainCatTransform.DOMove(point.position.XYOnly(), _moveDuration)
                .SetSpeedBased()
				.SetEase(_ease)
				.SetLink(_mainCatTransform.gameObject)
				.OnComplete(() =>
				{
					_sequence?.Kill();
					
					_modelTransform.DOKill();
					_modelTransform.DOLocalRotate(Vector3.zero, default);
					_modelTransform.DOScale(Vector3.one, default);
					
					action?.Invoke();
				});
		}
	}
}