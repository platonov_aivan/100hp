﻿using System.Collections.Generic;
using Extensions;
using Game.ScriptableObjects.Classes;
using Game.Scripts.Gameplay.Garden.GardenBed;
using Game.Scripts.Gameplay.Сats.MainCat;
using Game.Scripts.Gameplay.Сats.Queue;
using Game.Scripts.Gameplay.Сats.Queue.VisitorCats;
using Gameplay;
using UnityEngine;
using static Common.Enums;

namespace Game.Scripts.Gameplay.Reception
{
	public class ReceptionBase : MonoBehaviour
	{
		[Space]
		[Header("GardenBeds")]
		[SerializeField] private List<GardenBedMain> _gardenBedMains;
		[SerializeField] private UpgradeType _type;
		[Space]
		[Header("Queue")]
		[SerializeField] private Transform _spawnPointVisitor;
		[SerializeField] private Transform _removePointVisitor;
		[SerializeField] private List<ReceptionPoint> _receptionPoint;
		[SerializeField] private QueueCats _queueCats;
		

		private readonly List<GardenBedData> _gardenBedDatas = new();

		private void Start()
		{
			VisitorCat.OnProductRequest += ReceivingProduct;
			UpgradeProvider.OnUpgrade += BuyGardenBed;

			LoadData();
			_queueCats.Init(_spawnPointVisitor, _removePointVisitor, _receptionPoint);
		}

		private void OnDestroy()
		{
			VisitorCat.OnProductRequest -= ReceivingProduct;
			UpgradeProvider.OnUpgrade -= BuyGardenBed;
		}

		private void LoadData()
		{
			var countGardenBed = (int) UpgradeProvider.GetUpgradeValue(_type);

			for (var i = 0; i < countGardenBed; i++)
			{
				_gardenBedMains[i].Activate();
			}
		}

		private void BuyGardenBed(UpgradeType type)
		{
			if (_type == type)
			{
				LoadData();
			}
		}

		private void ReceivingProduct(VisitorCat visitorCat)
		{
			foreach (var gardenBedMain in _gardenBedMains)
			{
				if (!gardenBedMain.IsActivate) continue;
				if (!_gardenBedDatas.Contains(gardenBedMain.Data))
				{
					_gardenBedDatas.Add(gardenBedMain.Data);
				}
			}

			visitorCat.SetProduct(_gardenBedDatas.GetRandomElement());
		}
	}
}