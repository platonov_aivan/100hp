﻿using UnityEngine;

namespace Game.Scripts.Gameplay.Reception
{
	public class ReceptionPoint : MonoBehaviour
	{
		[field: SerializeField] public Transform PointTransform { get; set; }
		[field: SerializeField] public Transform MainCatPointTransform { get; set; }
		[field: SerializeField] public bool IsBusy { get; set; }
	}
}