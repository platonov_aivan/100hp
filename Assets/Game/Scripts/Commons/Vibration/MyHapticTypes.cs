namespace Common
{
	public enum MyHapticTypes
	{
		LightImpact,
		MediumImpact,
		HeavyImpact,
		RigidImpact,
		SoftImpact,
		Selection,
		Failure,
		Success,
		Warning,
	}
}