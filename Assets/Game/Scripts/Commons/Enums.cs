﻿namespace Common
{
	public static class Enums
	{
		public enum Axis
		{
			X,
			Y,
			Z
		}

		public enum AdsResult
		{
			None,
			Success,
			NotAvailable,
			Start,
			Fail,
			Watched,
			Canceled,
			Clicked
		}

		public enum ResourceType
		{
			Money,
			Resource2,
			Resource3,
		}
		public enum ProductType
		{
			Strawberry = 0,
			Blueberry = 1,
		}

		public enum UpgradeType
		{
			ImprovementGardenBed,
			Blueberry,
			Helper,
			AddGardenBed,
			Strawberry,
		}

		public enum AIState
		{
			GetResource,
			Waiting,
			Payment,
			Exit
		}

		public enum ResourceCarryType
		{
			Default,
			Follow
		}

		public enum ParticleType
		{
			None = 0,
			Smoke = 1,
			ExplosionPart = 2,
			Saw = 3,
			WeaponSmoke = 4,
			Tow = 5,
			PlayerBurning = 6,
			Exhaust = 7,
			EngineExplosion = 8,
			WeaponSparks = 9,
			Hit = 10,
		}

		public enum PartType
		{
			AllPart = 0,
			Wheels = 1,
			Cabin = 2,
			Carcase = 3,
			Weapon = 4,
		}
		
		public enum EnemyType
		{
			None = 0,
			Car = 1,
			Water = 2,
			Wall = 3,
		}

		public enum TeamType
		{
			None = 0,
			Player = 1,
			Enemy = 2,
		}

		public enum CharacterType
		{
			None = 0,
			Player = 1,
			Helper = 2,
			Buyer = 3
		}
	}
}